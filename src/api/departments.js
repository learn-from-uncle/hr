import request from '@/utils/request'

// 获取公司部门列表
export function getDeptsList() {
  return request({
    url: '/company/department'
  })
}

// 新增部门
export function addDepts(data) {
  return request({
    url: '/company/department',
    method: 'post',
    data
  })
}

// 删除部门
export function delDepts(id) {
  return request({
    url: `/company/department/${id}`,
    method: 'delete'
  })
}

// 根据id获取部门
export function getDeptsDetail(id) {
  return request({
    url: `/company/department/${id}`
  })
}

// 根据id修改部门
export function editDepts(data) {
  return request({
    url: `/company/department/${data.id}`,
    method: 'put',
    data
  })
}
