import request from '@/utils/request'

// 获取所有权限列表
export function getPermList() {
  return request({
    url: '/sys/permission'
  })
}

// 根据id获取权限详情
export function getPermDetail(id) {
  return request({
    url: '/sys/permission/' + id
  })
}

// 根据id更新权限
export function updatePerm(data) {
  return request({
    url: '/sys/permission/' + data.id,
    method: 'put',
    data
  })
}

// 新增员工权限
export function addPerm(data) {
  return request({
    url: '/sys/permission',
    method: 'POST',
    data
  })
}

// 根据id删除权限
export function delPerm(id) {
  return request({
    url: '/sys/permission/' + id,
    method: 'delete'
  })
}
