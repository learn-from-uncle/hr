import request from '@/utils/request'

// 根据id获取公司信息
export function getCompanyInfo(id) {
  return request({
    url: '/company/' + id
  })
}

// 获取角色列表
export function getRoleList(params) {
  return request({
    url: '/sys/role',
    // 如果接口文档规定query,请求url后面参加使用？name='jack'，这种形式，需要使用params来设置
    params
  })
}

// 新增角色
export function addRole(data) {
  return request({
    method: 'post',
    url: '/sys/role',
    // 如果接口文档规定参数为body，参数添加到请求体中，需要用data
    data
  })
}

// 通过id去获取角色信息
export function getRoleDetail(id) {
  return request({
    url: '/sys/role/' + id
  })
}

// 更新角色信息
export function editRole(data) {
  return request({
    url: '/sys/role/' + data.id,
    method: 'put',
    data
  })
}

// 删除角色
export function delRole(id) {
  return request({
    url: '/sys/role/' + id,
    method: 'delete'
  })
}

// 分配角色
export function assignPerm(data) {
  return request({
    method: 'put',
    url: '/sys/role/assignPrem',
    data
  })
}
