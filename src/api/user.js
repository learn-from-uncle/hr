import request from '@/utils/request'

// 登录接口
export function login(data) {
  return request({
    // 修改登录接口
    url: '/sys/login',
    method: 'post',
    data
  })
}

// 获取个人简单信息接口
export function getProfile() {
  return request({
    url: '/sys/profile',
    method: 'post'
  })
}

// 获取员工详情信息接口
export function getUserDetailById(id) {
  return request({
    url: `/sys/user/${id}`
  })
}

export function getInfo(token) {
  return request({
    url: '/vue-admin-template/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/vue-admin-template/user/logout',
    method: 'post'
  })
}
