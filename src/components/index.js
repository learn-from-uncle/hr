// 这里是自己封装的插件库
import PageTools from '@/components/PageTools'
// 引入花裤衩封装好的上传表格组件，一键赋值到UploadExcel的index中
import UploadExcel from '@/components/UploadExcel'
// 引入上传组件
import ImageUpload from '@/components/ImageUpload'
import Screenfull from '@/components/ScreenFull'
import ThemePicker from '@/components/ThemePicker'
// 暴露出去一个对象
export default {
// 插件必须有install方法，在插件被use时自动执行，默认情况下会接收到一个Vue构造器
  install(Vue) {
    Vue.component('PageTools', PageTools)
    Vue.component('UploadExcel', UploadExcel)
    Vue.component('ImageUpload', ImageUpload)
    Vue.component('Screenfull', Screenfull)
    Vue.component('ThemePicker', ThemePicker)
  }
}
