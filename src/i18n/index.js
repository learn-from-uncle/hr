import Vue from 'vue'
// 引入多语言包
import VueI18n from 'vue-i18n'

// 引入elemetnUi的语言包,第三方包可以直接选到包名 'element-ui/lib/locale/lang/zh-CN'
import elementZH from '../../node_modules/element-ui/lib/locale/lang/zh-CN'
import elementEN from '../../node_modules/element-ui/lib/locale/lang/en'

// 引入自己的语言包
import menusZH from './zh'
import menusEN from './en'

// 安装插件
Vue.use(VueI18n)
// 实例化
export default new VueI18n({
  locale: localStorage.getItem('lang') || 'zh',
  messages: {
    'zh': {
      // 拼接几个语言包
      ...elementZH,
      ...menusZH,
      hello: '你好，世界！',
      btnOK: '确定',
      btnCancel: '取消'
    },
    'en': {
      ...elementEN,
      ...menusEN,
      hello: 'Hello World!',
      btnOK: 'OK',
      btnCancel: 'Cancel'
    }
  }
})
