import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// 引入element-ui的英语包？
// import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import '@/icons' // icon
import '@/permission' // permission control

// 引入封装的i18n
import i18n from '@/i18n'

// 其他插件的语言设置
// 设置英语
// Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
Vue.use(ElementUI)

Vue.config.productionTip = false

// 自定义指令和使用
Vue.directive('fiximg', {
  inserted(el, binding) {
    // 自定义指令传参可以通过binding.value来接收，如： el.src = el.src || binding.value，需要在页面跳转之前先获取个人数据，使用async await,或者用update
    // 使用自定义指令，如：v-fiximg="require('@/assets/common/head.jpg')""
    // el是使用指令的dom
    console.log(el)
    // 处理空图片
    // 在js中，@会被原样输出,必须使用require
    console.log(32, el.src)
    // el.src = el.src || require('@/assets/common/head.jpg')
    el.src = el.src || binding.value
    // 处理错误路径图片
    // 监听图片的错误事件
    el.addEventListener('error', () => {
      el.src = require('@/assets/common/head.jpg')
    })
  },
  update(el, binding) {
    // 数据更新了就会触发
    // el.src = el.src || require('@/assets/common/head.jpg')
    el.src = el.src || binding.value
  }
})

// 全局左侧员工页面的头部部分，其他页面也要用
// import PageTools from '@/components/PageTools'
// Vue.component('PageTools', PageTools)

// vue的插件使用机制
// Vue.use()里面的一个对象
// 将所有的插件封装到components的index中，不用每次一个一个引入，减少main.js和组件的依赖（耦合）
import myPlugin from '@/components'
Vue.use(myPlugin)

// 平常的单个过滤器
// Vue.filter('formatDate', (oldVal) => {
//   const date = new Date(oldVal)
//   return date.toLocaleDateString()
// })

// 单个引入多个自定义过滤器中的一个
// import { formatDate } from './filters'
// Vue.filter('formatDate', formatDate)

// 当要大量引入过滤器的时候，我们希望一次性引入所有的过滤器
import * as myFilters from '@/filters/index' // 这种引入可以生成一个新对象，index中的函数名作为键，函数体作为值，组成新对象的键值对
// 遍历对象，循环注册过滤器
for (var key in myFilters) {
  Vue.filter(key, myFilters[key])
}

// 引入混入的用法
import mixin from '@/mixin'
Vue.mixin(mixin)

// 注入到根实例中
new Vue({
  el: '#app',
  router,
  store,
  i18n,
  render: h => h(App)
})
