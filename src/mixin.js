export default {
  // 混入的对象配置和vue组件的一样
  // 一旦全局混入,就相当于在每个组件都创建了同样的配置
  methods: {
    checkPerm(pointName) {
      // 获取二级权限
      // 使用es6的新语法,可选链符号?.
      // 可选链的使用,可选链前面有东西才执行后面的,如果前面为undefined或者null,则直接返回undefined/null
      const points = this.$store.state.user.profile.roles?.points
      // 返回布尔值
      return points?.includes(pointName)
    }
  }
}
