// 引入路由
import router from '@/router/index'

// 引入仓库
import store from '@/store/index'

// 导航守卫，由一个页面跳转到别的页面就会触发
router.beforeEach(async(to, from, next) => {
  // 可以next后加上return优化
  // 第一种写法
  console.log(11, store.getters.token)
  if (store.getters.token) {
    // 已经登录
    if (to.path === '/login') {
      // 已经登录后不能再去登录，默认去到主页面
      next('/')
    } else {
      // 登录了去别的页面
      // 这里调用获取个人信息接口，保存个人数据,这里注意要加上模块名
      // 不是每次跳转都要调用，判断profile这个空对象是否有键值对，注意{}的布尔值是true
      if (Object.keys(store.state.user.profile).length === 0) {
        // 确保获取完个人数据之后在跳转
        // 这里再加上try catch
        await store.dispatch('user/getProfile')
        // 在过滤动态路由
        // 获取用户数据中的memus
        const menus = store.state.user.profile.roles.menus
        // 调用permission里面的过滤路由函数,这里是异步,要加上await
        const res = await store.dispatch('permission/filterRoutes', menus)
        // 路由原生添加方法
        // 解决404问题,这里需要放到拼接动态路由的后面,不然刷新非主页会404,因为顺序问题
        // { path: '*', redirect: '/404', hidden: true }
        router.addRoutes([
          ...res,
          { path: '*', redirect: '/404', hidden: true }
        ])
        // 解决空白问题,阻止下面的next()
        return next(to.path)
      }
      next()
    }
  } else {
    // 未登录
    // 白名单
    const whiteList = ['/login', '/404', '/test']
    if (whiteList.indexOf(to.path) !== -1) {
      // 在白名单里面
      next()
    } else {
      // 不在白名单
      next('/login')
    }
  }

//   // 第二种写法
//   const url = to.path
//   const token = store.getters.token
//   const whiteList = ['/login', '/404', '/test']
//   // 1、登录
//   // 1、1去登录页面=>跳到主页
//   if (token && url === '/login') {
//     next('/')
//     return
//   }
//   // 1、2去其他页面=>放行
//   if (token && url !== '/login') {
//     next()
//     return
//   }
//   // 2、未登录
//   // 2、1在白名单中=>放行
//   if (!token && whiteList.indexOf(url) !== -1) {
//     next()
//     return
//   }
//   // 2、2不在白名单=>去登录页面
//   if (!token && whiteList.indexOf(url) === -1) {
//     next('/login')
//   }
})

