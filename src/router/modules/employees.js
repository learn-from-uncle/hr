import Layout from '@/layout'
export default {
  path: '/employees',
  name: 'employees',
  component: Layout,
  children: [{
    path: '',
    component: () => import('@/views/employees/index'),
    meta: { title: '员工', icon: 'people' }
  },
  {
    path: 'detail/:id', // 注意，二级部门不用拼接一级部门，这里如果加上/，点击那边要加上/，就是$router.push('/detail'),不然就是加上一级路由，this.$router.push('/employees/detail)
    component: () => import('@/views/employees/detail'),
    meta: { title: '员工详情', icon: 'tree' },
    hidden: true
  },
  {
    path: 'print/:id', // 注意，二级部门不用拼接一级部门，这里如果加上/，点击那边要加上/，就是$router.push('/detail'),不然就是加上一级路由，this.$router.push('/employees/detail)
    component: () => import('@/views/employees/print'),
    meta: { title: '打印详情', icon: 'tree' },
    hidden: true
  }
  ]
}
