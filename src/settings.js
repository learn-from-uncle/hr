module.exports = {

  // 修改标题
  title: '人力资源管理系统',

  /**
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  fixedHeader: false,

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  // 是否显示左侧图片变量
  sidebarLogo: true
}
