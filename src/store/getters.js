const getters = {
  // 利用getters简化获取数据
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  staffPhoto: state => state.user.profile.staffPhoto,
  name: state => state.user.profile.username,
  companyId: state => state.user.profile.companyId
}
export default getters
