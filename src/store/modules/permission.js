// 引入静态和动态路由
import { asyncRoutes, constantRoutes } from '@/router'
const state = {
  // 记录静态和动态路由,state中的变量只能通过mutations来改
  routes: []
}
const mutations = {
  setRoutes(state, data) {
    // 展开拼接
    state.routes = [
      ...constantRoutes,
      ...data
    ]
  }
}
const actions = {
  // 过滤路由函数
  filterRoutes(store, menus) {
    // 过滤
    const routes = asyncRoutes.filter(route => {
      // 如果true或者fasle,这里遍历每个route,如果它的name存在于menus中,就是true
      return menus.includes(route.name)
    })
    console.log('路由结果为:', routes)
    // 调用mutations中的拼接静态和动态路由函数
    store.commit('setRoutes', routes)
    // 返回过滤后的数据
    return routes
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
