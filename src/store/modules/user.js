// 引入user相关接口
// actions封装登录获取token，调用mutations设置state的token
import { login, getProfile, getUserDetailById } from '@/api/user'

// 引入路由
import router from '@/router'

// 使用cookie持久化token,旧项目/后端要求才使用，有时效和只有4kb左右大小
// 引入utils/auth.js中封装好的两个方法
import { getToken, setToken, removeToken } from '@/utils/auth'

const state = {
// 注意有时候存储返回的字符串，即使是'undefined',判断也是true

  // 1、localStorage解决刷新重置token,判断是否有本地
  // token: localStorage.getItem('token') || null

  // 2、使用cookie读取存储本地
  token: getToken() || '',
  profile: {} // 保存个人信息
}
const mutations = {

  // 设置state的token
  setToken(state, data) {
    // 1、localStorage本地存储token
    // localStorage.setItem('token', data)
    // 2、cookie本地存储
    setToken(data)
    state.token = data
  },
  // 保存state的个人数据
  setProfile(state, data) {
    state.profile = data
  },
  // 清理和重置token
  removeToken(state) {
    // 清理cookie的token
    removeToken()
    // 重置state的token
    state.token = ''
  },
  // 清理个人信息
  removeProfile(state) {
    state.profile = {}
  }
}
const actions = {
  async login(store, data) {
    // 登录接口获取数据
    const res = await login(data)
    store.commit('setToken', res)
    // 记录登录成功的时间戳
    localStorage.setItem('loginTime', Date.now())
    console.log(res)
  },
  async getProfile(store) {
    // 调用获取个人信息
    const res = await getProfile()
    // 调用获取员工详情
    const detail = await getUserDetailById(res.userId)
    // 展开合并成一个对象
    store.commit('setProfile', {
      ...res,
      ...detail
    })
  },
  // 封装退出
  logout(store) {
    store.commit('removeToken')
    store.commit('removeProfile')
    router.push('/login')
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

