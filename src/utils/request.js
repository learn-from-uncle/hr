// 引入库
import axios from 'axios'
// 单独引入elementUI中的Message
import { Message } from 'element-ui'

// 引入store
import store from '@/store/index'

// 创建实例
const service = axios.create({
  // 配置基准路径，最终url=baseURL+接口url
  // VUE_APP_BASE_API是环境变量，.env.development和.env.production和.env.staging
  // 只有 NODE_ENV，BASE_URL 和以 VUE_APP_ 开头的变量将通过 webpack.DefinePlugin 静态地嵌入到客户端侧的代码中
  baseURL: process.env.VUE_APP_BASE_API,
  // 超时设定
  timeout: 5000
})

// 啥都没做的请求拦截器
service.interceptors.request.use(config => {
  // 判断是否有token，如果有就给所有的请求加上token
  const token = store.getters.token
  if (token) {
    // 判断发起请求的时间减去登录的时间，是否超过某个时间
    // 2两个小时超时
    const timeout = 1000 * 60 * 60 * 2
    const loginTime = localStorage.getItem('loginTime')
    const now = Date.now()
    const isTimeOut = now - loginTime > timeout
    if (isTimeOut) {
      store.dispatch('user/logout')
      return Promise.reject(new Error('登录超时'))
    }
    // 标准写法，加上'Bearer ',注意有空格
    config.headers.Authorization = 'Bearer ' + token
  }
  return config
})

// 啥都没做的响应拦截器
// 响应拦截器，可以接收两个参数，都是回调函数
// 第一个拦截成功200
// 第二个拦截其他错误
service.interceptors.response.use(res => {
  // 解构
  const { success, message } = res.data
  if (success) {
    // 简化使用请求的返回数据
    return res.data.data
  } else {
    console.log('数据层面的错误')
    // 弹窗提醒
    Message.error(message)
    // 使用原生Promise.reject()来马上停止当前异步操作，不然错误了还会执行后面，使用可以直接跳到try,catch的catch里面，参数传入可以原生的new Error()整个错误对象，用catch接收
    // 错误对象可以追踪错误
    return Promise.reject(new Error(message))
  }
}, err => {
  console.log('网络层面的错误')
  // 第二个回调函数中err就是一个错误对象
  // 打印一个错误对象可以用console.dir()
  console.dir(err)
  // 被动处理token失效的问题
  // 这里需要注意，并不是所有的网络错误都有response
  if (err.response && err.response.data.code === 10002) {
    store.dispatch('user/logout')
  }
  Message.error(err.message)
  return Promise.reject(err)
})

// 导出/暴露实例
export default service
