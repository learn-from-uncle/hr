// 这个是后端或者运维的文件，创建页面服务配置，上线，是单独的，不是放在这个项目文件夹的
// 创建文件夹webServer
// 初始化npm init -y
// 创建index.js文件
// 下载引入模块koa和koa-static
const Koa = require('koa')
const serve = require('koa-static')
// 引入koa2-connect-history-api-fallback的封装
const { historyApiFallback } = require('koa2-connect-history-api-fallback')
const proxy = require('koa2-proxy-middleware')

// 创建服务器
const app = new Koa()

// 生成环境下的跨域问题
// 下载引入npm i 'koa2-proxy-middleware'
app.use(proxy({
  // 代理配置
  targets: {
    '/prod-api/(.*)': {
      // target:'http://ihrm.itheima.net',
      target: 'http://ihrm-java.itheima.net',
      changeOrigin: true,
      // 跟原来开发服务器不一样的地方是，开发环境配置url是'/prod-api,虽然可以触发代理，但是后端数据库不认可，后端只认api,需要做路径重写
      pathRewrite: {
        '/prod-api': 'api'
      }
    }
  }
}))

// 提供服务之前，调用中间件解决刷新的问题
app.use(historyApiFallback())

// 创建静态页面服务器服务中间件，还可以用express等多种方法来，配置服务地址,这里模拟打包后dist的所有文件放到web文件夹下
app.use(serve(__dirname + '/web'))

// 开启和监听服务器
app.listen('3333', () => {
  console.log('服务器已经开启：')
  console.log('http://localhost:3333')
})

// 解决刷新 not found 问题,因为前端设置了history模式
// 下载引入使用koa2-connect-history-api-fallback
